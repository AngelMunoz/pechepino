<?php
declare (strict_types = 1);

use App\Actions\Auth\LoginAction;
use App\Actions\Auth\SignupAction;
use App\Actions\User\ListUsersAction;
use App\Actions\User\ViewUserAction;
use App\Middleware\JwtMiddleware;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $container = $app->getContainer();
    $settings = $container->get('settings');
    $logger = $container->get(LoggerInterface::class);

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });

    $app->group('/auth', function (Group $group) use ($container) {
        $group->post('/login', LoginAction::class);
        $group->post('/signup', SignupAction::class);
    });

    $app->group('/api/users', function (Group $group) use ($container) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
    })->addMiddleware(new JwtMiddleware($logger, $settings['jwt']));
};
