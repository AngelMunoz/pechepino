<?php
declare (strict_types = 1);

use Middlewares\TrailingSlash;
use Slim\App;

return function (App $app) {
    $app->add(new TrailingSlash(true));
};
