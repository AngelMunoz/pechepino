<?php
declare (strict_types = 1);

use App\Interfaces\IJwtService;
use App\Interfaces\IUserService;
use App\Services\JwtService;
use App\Services\UserService;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our IUserService interface to its in memory implementation
    $containerBuilder->addDefinitions([
        IUserService::class => function (ContainerInterface $c) {
            $db = $c->get('db');
            $table = $c->get('db')->table('users');
            return new UserService($table);
        },
        IJwtService::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');
            $service = new JwtService($settings['jwt']['JWT_KEY']);
            return $service;
        },
    ]);
};
