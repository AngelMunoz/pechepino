<?php
declare (strict_types = 1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // Should be set to false in production
            'logger' => [
                'name' => 'slim-app',
                'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                'level' => Logger::DEBUG,
            ],
            'db' => [
                'driver' => isset($_ENV['DB_DRIVER']) ? $_ENV['DB_DRIVER'] : 'mysql',
                'host' => isset($_ENV['DB_HOST']) ? $_ENV['DB_HOST'] : '127.0.0.1',
                'database' => isset($_ENV['DB_NAME']) ? $_ENV['DB_NAME'] : 'pechepino',
                'username' => isset($_ENV['DB_USERNAME']) ? $_ENV['DB_USERNAME'] : 'root',
                'password' => isset($_ENV['DB_PASSWORD']) ? $_ENV['DB_PASSWORD'] : 'Admin123',
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
            ],
            'jwt' => [
                'JWT_KEY' => isset($_ENV['JWT_KEY']) ? $_ENV['JWT_KEY'] : 'todo secreto av',
            ],
        ],
    ]);
};
