# Slim Framework 4 Skeleton Application

Use this skeleton application to quickly setup and start working on a new Slim Framework 4 application. This application uses the latest Slim 4 with Slim PSR-7 implementation and PHP-DI container implementation. It also uses the Monolog logger.

This skeleton application was built for Composer. This makes setting up a new Slim Framework application quick and easy.

* Point your virtual host document root to your new application's `public/` directory.
* Ensure `logs/` is web writable.

To run the application in development, you can run these commands 

```bash
cd [my-app-name]
composer start
```

Or you can use `docker-compose` to run the app with `docker`, so you can run these commands:
```bash
cd [my-app-name]
docker-compose up -d
```
After that, open `http://0.0.0.0:8080` in your browser.

You should have a mysql or another sql database before trying to log in/sign up
you can customize that with the following env vars
```
PHP_ENVIRONMENT=development
DB_DRIVER=mysql
DB_HOST=127.0.01
DB_NAME=pechepino
DB_USERNAME=root
DB_PASSWORD=Admin123
JWT_KEY=a really good secret key instead of this text
```