<?php
declare (strict_types = 1);

namespace App\Actions\Auth;

use App\Exceptions\WrongCredentialsException;
use Psr\Http\Message\ResponseInterface as Response;

class LoginAction extends AuthAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $payload = $this->request->getParsedBody();

        $this->logger->debug("Login in as {$payload['username']}");

        $user = $this->userService->findOneByUsername($payload['username']);

        if (!\password_verify($payload['password'], $user->password)) {
            throw new WrongCredentialsException($this->request);
        }

        $token = $this->jwt->generateToken(['username' => $user->username]);

        $this->logger->info("Users list was viewed.");

        return $this->respondWithData([
            'username' => $user->username,
            'token' => $token,
        ]);
    }
}
