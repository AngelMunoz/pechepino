<?php
declare (strict_types = 1);

namespace App\Actions\Auth;

use App\Models\User;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

class SignupAction extends AuthAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $payload = $this->request->getParsedBody();

        $this->logger->debug("Signin up as {$payload['username']}");
        $user = null;
        try {
            $user = $this->userService->findOneByUsername($payload['username']);
        } catch (\Throwable $th) {}
        if (!is_null($user)) {
            throw new HttpBadRequestException($this->request, 'Username Already Taken');
        }

        $user = new User(null, $payload);
        $this->logger->info("Validating {$user->getUsername()}.");

        if (!$user->validate()) {
            throw new HttpBadRequestException($this->request, 'Please check username, first name, last name and password');
        }

        $user = $this->userService->create($user);

        $token = $this->jwt->generateToken(['username' => $user->getUsername()]);

        return $this->respondWithData([
            'username' => $user->getUsername(),
            'token' => $token,
        ]);

    }
}
