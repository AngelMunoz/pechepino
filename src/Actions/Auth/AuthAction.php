<?php
declare (strict_types = 1);

namespace App\Actions\Auth;

use App\Actions\Action;
use App\Actions\Auth;
use App\Interfaces\IJwtService;
use App\Interfaces\IUserService;
use Psr\Log\LoggerInterface;

abstract class AuthAction extends Action
{
    /**
     * @var IUserService
     */
    protected $userService;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var IJwtService
     */
    protected $jwt;

    /**
     * @param LoggerInterface $logger
     * @param IUserService  $userService
     * @param IJwtService  $jwtService
     */
    public function __construct(LoggerInterface $logger, IUserService $userService, IJwtService $jwtService)
    {
        parent::__construct($logger);
        $this->userService = $userService;
        $this->logger = $logger;
        $this->jwt = $jwtService;
    }
}
