<?php
declare (strict_types = 1);

namespace App\Interfaces;

interface IJwtService
{
    /**
     * @param array $payload
     * @param array $opts
     */
    public function generateToken($payload, $opts);

    public function decodeToken(string $token);
}
