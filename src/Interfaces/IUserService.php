<?php
declare (strict_types = 1);

namespace App\Interfaces;

use App\Models\User;

interface IUserService
{
    /**
     * @param int $page
     * @param int $limit
     * @return int|User[]
     */
    public function find(int $page, int $limit);

    /**
     * @param int $id
     * @return User
     * @throws UserNotFoundException
     */
    public function findOne(int $id);

    /**
     * @param string $email
     * @return User
     * @throws UserNotFoundException
     */
    public function findOneByUsername(string $email);

    /**
     * @param User $user
     * @return User
     */
    public function create(User $user);
}
