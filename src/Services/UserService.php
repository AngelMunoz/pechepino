<?php

declare (strict_types = 1);

namespace App\Services;

use App\Exceptions\UserNotFoundException;
use App\Interfaces\IUserService;
use App\Models\User;
use Illuminate\Database\Query\Builder;

class UserService implements IUserService
{
    /**
     * @var User[]
     */
    private $users;

    /**
     * @var Illuminate\Database\Query\Builder
     */
    protected $table;

    /**
     * UserService constructor.
     *
     * @param array|null $users
     */
    public function __construct(Builder $table)
    {
        $this->table = $table;
    }

    /**
     * {@inheritdoc}
     */
    public function find(int $page = 1, $limit = 10)
    {
        $offset = ($page - 1) * $limit;
        $collection = $this->table
            ->select('id', 'username', 'firstName', 'lastName')
            ->offset($offset)
            ->limit($limit)
            ->get()->all();

        $count = $this->table->count();

        return ['count' => $count, 'list' => $collection];
    }

    /**
     * {@inheritdoc}
     */
    public function findOne(int $id)
    {
        $collection = $this->table
            ->where('id', $id)
            ->select('id', 'username', 'firstName', 'lastName')
            ->get();
        if ($collection->isEmpty()) {
            throw new UserNotFoundException;
        }
        return $collection->first();
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByUsername(string $username)
    {
        $collection = $this->table
            ->where('username', $username)
            ->select('id', 'username', 'firstName', 'lastName')
            ->get();
        if ($collection->isEmpty()) {
            throw new UserNotFoundException;
        }
        return $collection->first();
    }

    /**
     * {@inheritdoc}
     */
    public function create(User $user)
    {
        $hash = \password_hash($user->getPassword(), PASSWORD_BCRYPT, ['cost' => 10]);
        $user->setPassword($hash);
        $insert = $user->jsonSerializeWithPassword();
        unset($insert['id']);
        $id = $this->table->insertGetId($insert);
        $result = (array) $this->table
            ->select('id', 'username', 'firstName', 'lastName')
            ->find($id);
        return new User($result['id'], $result);
    }
}
