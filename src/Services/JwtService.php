<?php

declare (strict_types = 1);

namespace App\Services;

use App\Interfaces\IJwtService;
use Firebase\JWT\JWT;

class JwtService implements IJwtService
{

    /**
     * @var string
     */
    protected $jwtKey;

    public function __construct(string $jwtKey)
    {
        if (empty($jwtKey)) {
            throw new ArgumentException('param $jwtKey should not be null');
        }
        $this->jwtKey = $jwtKey;
        JWT::$leeway = 120;
    }

    public function generateToken($payload = [], $opts = [])
    {
        $token = \array_merge($payload, [
            "iat" => \time(),
            "nbf" => \time(),
            "exp" => \time() + (24 * 60 * 60),
        ]);
        return JWT::encode($token, $this->jwtKey);
    }

    public function decodeToken(string $token)
    {
        return (array) JWT::decode($token, $this->jwtKey, array('HS256'));
    }

}
