<?php
declare (strict_types = 1);

namespace App\Exceptions;

use Slim\Exception\HttpUnauthorizedException;

class InvalidJwtException extends HttpUnauthorizedException
{
    public $message = "The JWT in the request was missing or was invalid";
}
