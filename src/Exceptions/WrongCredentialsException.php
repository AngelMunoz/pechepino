<?php
declare (strict_types = 1);

namespace App\Exceptions;

use Slim\Exception\HttpUnauthorizedException;

class WrongCredentialsException extends HttpUnauthorizedException
{
    public $message = "Provided credentials are not valid";
}
