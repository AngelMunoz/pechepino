<?php
declare (strict_types = 1);

namespace App\Exceptions;

class RecordNotFoundException extends \Exception
{
    public $message = 'The record you requested does not exist.';
}
