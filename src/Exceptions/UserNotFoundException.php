<?php
declare (strict_types = 1);

namespace App\Exceptions;

class UserNotFoundException extends RecordNotFoundException
{
    public $message = 'The user you requested does not exist.';
}
