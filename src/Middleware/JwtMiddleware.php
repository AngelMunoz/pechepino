<?php
declare (strict_types = 1);

namespace App\Middleware;

use App\Exceptions\InvalidJwtException;
use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface as Middleware;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Psr\Log\LoggerInterface;

class JwtMiddleware implements Middleware
{

    protected $jwtSettings;
    protected $logger;

    public function __construct(LoggerInterface $logger, array $jwtSettings = [])
    {
        $this->logger = $logger;
        if (empty($jwtSettings)) {
            throw new ArgumentException('param jwtSettings should not be null');
        }
        $this->jwtSettings = $jwtSettings;
    }
    /**
     * {@inheritdoc}
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
        $this->logger->info("Checking JWT Header for {$request->getUri()}");
        $authHeader = $request->getHeader('Authorization');
        if (empty($authHeader)) {
            throw new InvalidJwtException($request);
        }
        try {
            $exploded = explode(' ', $authHeader[0]);
            if (array_key_exists(1, $exploded)) {
                JWT::decode($exploded[1], $this->jwtSettings['JWT_KEY'], array('HS256'));
            } else {
                throw new InvalidJwtException($request);
            }
        } catch (\Throwable $e) {
            $this->logger->error($e->getMessage());
            throw new InvalidJwtException($request);
        }
        return $handler->handle($request);
    }
}
