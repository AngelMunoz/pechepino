<?php
declare (strict_types = 1);

namespace App\Models;

use JsonSerializable;

class User implements JsonSerializable
{
    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string;
     */
    private $password;

    /**
     * @param int|null  $id
     * @param array $opts
     */
    public function __construct(?int $id, array $opts)
    {
        $this->id = $id;
        $this->username = isset($opts['username']) ? $opts['username'] : null;
        $this->firstName = isset($opts['firstName']) ? $opts['firstName'] : null;
        $this->lastName = isset($opts['lastName']) ? $opts['lastName'] : null;
        $this->password = isset($opts['password']) ? $opts['password'] : null;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return strtolower($this->username);
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return ucfirst($this->firstName);
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return ucfirst($this->lastName);
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function validate(): bool
    {
        if (empty($this->password)) {return false;}
        $uppercase = preg_match('@[A-Z]@', $this->password);
        $lowercase = preg_match('@[a-z]@', $this->password);
        $number = preg_match('@[0-9]@', $this->password);
        return !(
            empty($this->username) ||
            empty($this->firstName) ||
            empty($this->lastName) ||
            //
            !$uppercase || !$lowercase || !$number || strlen($this->password) < 6
        );
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
        ];
    }

    /**
     * @return array
     */
    public function jsonSerializeWithPassword()
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'password' => $this->password,
        ];
    }
}
